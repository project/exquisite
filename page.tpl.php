<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <!--[if IE 6]><link rel="stylesheet" href="<?php echo $base_path . $directory; ?>/style.ie6.css" type="text/css" /><![endif]-->
</head>

<body class="<?php print $body_classes; ?>">
<div id="page">
	<div id="header">
		<div id="headerInner">
			<?php if ($site_name) { ?><h1><a href="<?php print $front_page ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
			<?php if ($site_slogan) { ?><h2><?php print $site_slogan ?></h2><?php } ?>
		</div>
	</div>

<?php if (!empty($navigation)): ?>
	<div class="nav">
		<div class="l"></div>
		<div class="r"></div>
		<?php echo $navigation; ?>
	</div>
<?php endif; ?>
	<div id="navigation"> </div>
	<div id="container">
		<?php if ($header) { ?><div id="precontent"><?php print $header ?></div><?php } ?>

		<?php if ($left) { ?>
			<div id="sidebar-left" class="sidebar">
				<?php print $left ?>
			</div>
		<?php } ?>

		<div id="main">
			<?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
			<?php if (!$is_front) print $breadcrumb ?>
			<?php if ($show_messages) { print $messages; } ?>
			<?php if ($tabs) { ?><div class="tabs"><?php print $tabs ?></div><?php } ?>
			<?php if ($title) { ?><h1 class="title"><?php print $title ?></h1><?php } ?>
			<?php print $help ?>
			<?php print $content; ?>
			<?php print $feed_icons; ?>
		</div>

		<?php if ($right) { ?>
			<div id="sidebar-right" class="sidebar">
				<?php print $right ?>
			</div>
		<?php } ?>
		<div style="clear: both;"> </div>

		<div id="postcontent"><?php print $footer ?><div style="clear: both;"> </div></div>
	</div>

	<div id="footer">
		<p><?php print $footer_message ?></p>
	</div>
</div>

<?php print $closure ?>
</body>
</html>