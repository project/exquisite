<?php // $Id$ ?>
<div id="block-<?php echo $block->module . '-' . $block->delta; ?>" class="clear-block block block-<?php echo $block->module; ?>">
  <?php if ($block->subject): ?>
    <h3 class="title"><?php echo $block->subject; ?></h3>
  <?php endif;?>
  <div class="content">
    <?php echo $block->content; ?>
  </div>
</div>
